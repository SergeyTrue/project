package by.rogat.web.project.service;

import by.rogat.web.project.entity.Cart;
import by.rogat.web.project.entity.Item;

public interface CartService {

    Cart createCart(Cart cart, Long userId);

    Cart addItemToCart(Long cartId, Long productId, Item item);

    Cart purchaseCart(Long cartId);
}

