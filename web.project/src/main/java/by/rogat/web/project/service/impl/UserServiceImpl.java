package by.rogat.web.project.service.impl;

import by.rogat.web.project.dto.UserDto;
import by.rogat.web.project.entity.User;
import by.rogat.web.project.repo.UserRepository;
import by.rogat.web.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {

    private static Logger log = Logger.getLogger(UserServiceImpl.class.getName());

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findById(Long id) {
        log.info("find by id user");
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User createUser(User user) {
        user.setBalance(1);
        log.info("create new User");
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
        log.info("delete User");
    }

    @Override
    public void update(Long id, UserDto userDto) {
        User user = userRepository.findById(id).orElse(null);
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        userRepository.save(user);
        log.info("update User login and password");
    }

    @Override
    public void updateBalance(Long id, UserDto userDto) {
        User user = userRepository.findById(id).orElse(null);
        user.setBalance(userDto.getBalance());
        userRepository.save(user);
        log.info("update User balance");
    }
}
