package by.rogat.web.project.dto;

import by.rogat.web.project.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class UserDto {

    private Long id;
    private String login;
    private String password;
    private Integer balance;

    public static UserDto toModel(User user) {
        UserDto model = new UserDto();
        model.setLogin(user.getLogin());
        model.setPassword(user.getPassword());
        return model;
    }

    public static UserDto toBalance(User user) {
        UserDto model = new UserDto();
        model.setBalance(user.getBalance());
        return model;
    }
}
