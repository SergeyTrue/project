package by.rogat.web.project.service.impl;

import by.rogat.web.project.entity.Product;
import by.rogat.web.project.repo.ProductRepository;
import by.rogat.web.project.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class ProductServiceImpl implements ProductService {

    private static Logger log = Logger.getLogger(ProductServiceImpl.class.getName());

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public Product addProduct(Product product) {
        log.info("create product");
        return productRepository.save(product);
    }

    @Override
    public Product findById(Long id) {
        log.info("find by id product");
        return productRepository.findById(id).orElse(null);
    }
}
