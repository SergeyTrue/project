package by.rogat.web.project.service;

import by.rogat.web.project.entity.Product;

public interface ProductService {

    Product addProduct(Product product);

    Product findById(Long id);

}
