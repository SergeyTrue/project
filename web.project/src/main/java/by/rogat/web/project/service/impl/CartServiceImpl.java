package by.rogat.web.project.service.impl;

import by.rogat.web.project.entity.Cart;
import by.rogat.web.project.entity.Item;
import by.rogat.web.project.entity.User;
import by.rogat.web.project.repo.CartRepository;
import by.rogat.web.project.repo.UserRepository;
import by.rogat.web.project.service.CartService;
import by.rogat.web.project.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.logging.Logger;

@Service
public class CartServiceImpl implements CartService {

    private static Logger log = Logger.getLogger(CartServiceImpl.class.getName());

    private final UserRepository userRepository;
    private final CartRepository cartRepository;
    private final ProductService productService;


    @Autowired
    public CartServiceImpl(UserRepository userRepository, CartRepository cartRepository, ProductService productService) {
        this.userRepository = userRepository;
        this.cartRepository = cartRepository;
        this.productService = productService;
    }

    @Override
    public Cart createCart(Cart cart, Long userId) {
        User user = userRepository.findById(userId).orElse(null);
        cart.setUser(user);
        cart.setStatus(false);
        log.info("create Cart");
        return cartRepository.save(cart);
    }

    @Override
    public Cart addItemToCart(Long cartId, Long productId, Item item) {
        Cart cart = cartRepository.findById(cartId).orElse(null);
        cart.getItems().add(Item.builder()
                .cart(cartRepository.getById(cartId))
                .product(productService.findById(productId))
                .quantity(item.getQuantity())
                .build());
        log.info("add Item To Cart");
        return cartRepository.save(cart);
    }

    @Transactional
    @Override
    public Cart purchaseCart(Long cartId) {
        Cart cart = cartRepository.findById(cartId).orElse(null);
        User user = cart.getUser();
        user.setBalance(user.getBalance() - cart.getItems().stream()
                .map(item1 -> item1.getQuantity() * item1.getProduct().getPrice())
                .reduce(Integer::sum)
                .orElse(0));
        userRepository.save(user);
        cart.setStatus(true);
        log.info("purchase Cart");
        return cartRepository.save(cart);
    }
}