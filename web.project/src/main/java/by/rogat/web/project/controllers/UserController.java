package by.rogat.web.project.controllers;

import by.rogat.web.project.dto.UserDto;
import by.rogat.web.project.entity.User;
import by.rogat.web.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {


    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user")
    public ResponseEntity<User> createUser(@RequestBody User user) {
        return ResponseEntity.ok(userService.createUser(user));
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<User> findUserById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Long> deleteUserById(@PathVariable Long id) {
        userService.findById(id);
        userService.deleteUser(id);
        return ResponseEntity.ok(id);
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Long> update(@PathVariable Long id, @RequestBody UserDto userDto) {
        userService.update(id, userDto);
        return ResponseEntity.ok(id);
    }

    @PutMapping("/user/balance/{id}")
    public ResponseEntity<Integer> updateBalance(@PathVariable Long id, @RequestBody UserDto userDto) {
        userService.updateBalance(id, userDto);
        return ResponseEntity.ok(userDto.getBalance());
    }
}
