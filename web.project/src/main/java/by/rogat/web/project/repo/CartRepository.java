package by.rogat.web.project.repo;

import by.rogat.web.project.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Long>, JpaRepository<Cart, Long> {
}
