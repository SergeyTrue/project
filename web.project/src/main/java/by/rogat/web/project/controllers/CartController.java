package by.rogat.web.project.controllers;

import by.rogat.web.project.entity.Cart;
import by.rogat.web.project.entity.Item;
import by.rogat.web.project.repo.CartRepository;
import by.rogat.web.project.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CartController {

    private final CartRepository cartRepository;
    private final CartService cartService;

    @Autowired
    public CartController(CartRepository cartRepository, CartService cartService) {
        this.cartRepository = cartRepository;
        this.cartService = cartService;
    }


    @PostMapping("cart/{userId}")
    public ResponseEntity<Cart> createCart(@RequestBody Cart cart,
                                           @PathVariable Long userId) {
        return ResponseEntity.ok(cartService.createCart(cart, userId));
    }

    @PutMapping("cart/{cartId}/{productId}")
    public ResponseEntity<Cart> addItem(
            @PathVariable Long cartId,
            @PathVariable Long productId,
            @RequestBody Item item) {
        Cart cart = cartService.addItemToCart(cartId, productId, item);
        return ResponseEntity.ok(cartRepository.save(cart));
    }

    @PostMapping("cart/purchase/{cartId}")
    public ResponseEntity<Cart> purchaseCart(
            @PathVariable Long cartId) {
        Cart cart = cartService.purchaseCart(cartId);
        return ResponseEntity.ok(cartRepository.save(cart));
    }
}
