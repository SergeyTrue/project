package by.rogat.web.project.service;

import by.rogat.web.project.dto.UserDto;
import by.rogat.web.project.entity.User;

public interface UserService {

    User findById(Long id);

    User createUser(User user);

    void deleteUser(Long id);

    void update(Long id, UserDto userDto);

    void updateBalance(Long id, UserDto userDto);

}
